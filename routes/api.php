<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [ 'as' => 'login', 'uses' => 'api\UsersController@login']);
//Route::post('login', 'api\UsersController@login');
Route::post('register', 'api\UsersController@register');
// Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'api\UsersController@currentUser');
    Route::post('account/sold', 'api\AccountsController@getSold');
    Route::get('account/transactions', 'api\AccountsController@history');
    Route::post('account/debit', 'api\AccountsController@debit');
    Route::post('account/history', 'api\AccountsController@history');
    Route::post('account/innerTransfert', 'api\AccountsController@innerTransfert');
    Route::resource('transferts', 'api\TransfertsController');  
    Route::resource('transactions', 'api\TransactionsController');
    Route::resource('users', 'api\UsersController'); 
    Route::resource('accounts', 'api\AccountsController');
    Route::post('account/externTransfert', 'api\AccountsController@externTransfert'); 
// });
Route::post('account/credit', 'api\AccountsController@credit');
//Route::resource('results', 'api\ResultsController');
//Route::resource('symptoms', 'api\SymptomsController');
//Route::resource('notifys', 'api\NotifysController');
//Route::resource('notifications', 'api\NotificationsController');
//Route::resource('usersNotifications', 'api\UserNotificationsController');
//Route::resource('userSymptoms', 'api\UserSymptomsController');
//Route::resource('users', 'api\UsersController');
