<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('bank')->nullable();
            $table->string('scope')->nullable();
            $table->bigInteger('ext_account_number')->nullable();
            $table->integer('account_id')->unsigned()->index()->nullable()->default(null);
            $table->integer('transfert_id')->unsigned()->index()->nullable()->default(null);
            $table->double('amount')->nullable();
            $table->string('date')->nullable();
            $table->boolean('edited')->default(false);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
