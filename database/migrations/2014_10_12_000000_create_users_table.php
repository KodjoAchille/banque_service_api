<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            //$table->string('username')->nullable();
            //$table->string('name')->nullable();
            $table->string('full_name')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('lastname')->nullable();
            $table->string('firstname')->nullable();
            //$table->string('blood_group')->nullable();
            //$table->string('disease_state')->nullable();
            //$table->integer('result_id')->unsigned()->index()->nullable();
            //$table->integer('notify_id')->unsigned()->index()->nullable();
            //$table->integer('parent_id')->unsigned()->index()->nullable();
            $table->string('email')->unique();
            $table->boolean('edited')->default(false);
            $table->boolean('deleted')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
