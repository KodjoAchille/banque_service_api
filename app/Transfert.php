<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfert extends Model
{
    //
    protected $table="transferts";
    protected $timestamp = true;

    public function transactions(){
        return $this->hasMany('App\Transaction');
    }
}
