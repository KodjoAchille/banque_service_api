<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class UsersController extends Controller 
{
    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus ,['user_id'=>$user->id]); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        
        //$input = $request->all(); 
        
        $user = new \App\User();
        
        $user->lastname=$request->lastname;
        $user->firstname=$request->firstname;
        $user->birth_place=$request->birth_place;
        $existEmail=\App\User::where('email',$request->email)->first();
        if($existEmail)
        {
            return response()->json([
                'message'=>"Email already used",
                'status'=>403
            ],403);
        }
        $user->email=$request->email;
        $user->birth_date=$request->birth_date;
        $user->city=$request->city;
        $user->country=$request->country;
        $user->password=bcrypt($request->password);
        $user->save();
        //création de compte
        $account = new \App\Account();
        $account->account_number=$this->generateUniqueCode();
        $account->code=$request->code;
        $account->sold=$request->sold;
        $account->type=$request->type;
        $account->user()->associate($user);
        $account->save();
        //return response()->json($account);
        //$success['token'] =  $user->createToken('MyApp')-> accessToken; 
        //$success['email'] =  $user->email;
        return response()->json([
            'user'=>$user,
            'account'=>$account
        ], $this-> successStatus); 
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function generateUniqueCode()
    {
        do {
            $account_number = random_int(10000000, 99999999);
        } while (\App\Account::where('account_number',$account_number)->first());
        return $account_number;
    }
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function currentUser() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users= \App\User::where('deleted', false)->with('accounts')->orderBy('id', 'DESC')->get();
        return response()->json([
            'users'=>$users
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user= \App\User::with('accounts')->find($id);

        return response()->json(['user'=> $user]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user= \App\User::find($id);

        if($request->full_name)
        {
            $user->full_name=$request->full_name;
        }
        if($request->birth_date)
        {
            $user->birth_date=$request->birth_date;
        }
        if($request->city)
        {
            $user->city=$request->city;
        }
        if($request->country)
        {
            $user->country=$request->country;
        }
        
        // if($request->parent_id)
        // {
        //     $parrent=\App\User::find($request->parent_id);
        //     if($parent)
        //     {
        //         $user->parent()->associate($parent);
        //     }
        // }
        $user->edited = 1;
        $user->save();
        $user= \App\User::with('accounts')->find($id);
        return response()->json($user);
    }
    
    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $user = new \App\User();
    //     $user->full_name=$request->full;
    //     $user->email=$request->email;
    //     $user->birth_date=$request->birth_date;
    //     $user->city=$request->city;
    //     $user->country=$request->country;
    //     $user->email=$request->email;
    //     $user->save();
    //     return response()->json($user);
    // }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \App\User::find($id);
        $user->deleted = 1;
        $user->save();
        return response()->json($user);
    }
    
}