<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Transaction; 

class TransactionsController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions= \App\Account::where('deleted', false)->with('transaction','account')->orderBy('id', 'DESC')->get();
        return response()->json([
            'transactions'=>$transactions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new \App\transaction();
        $transaction->type=$request->type;
        $transaction->amount=$request->amount;
        $transaction->date=$request->date;
        $account=\App\Account::find($request->account_id);
        if($account)
        {
            $transaction->account()->associate($account);
        }
        $transfert=\App\Transfert::find($request->transfert_id);
        if($transfert)
        {
            $transaction->transfert()->associate($transfert);
        }
        $transaction->save();
        return response()->json($transfert);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction= \App\Transaction::with('account','transfert')->find($id);

        return response()->json(['transaction'=> $transaction]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $account= \App\Account::find($id);
        if($request->type)
        {
            $transaction->type=$request->type;    
        }
        if($request->amount)
        {
            $transaction->amount=$request->amount;    
        }
        if($request->date)
        {
            $transaction->date=$request->date;    
        }
        if($request->account_id)
        {
            $account=\App\Account::find($request->account_id);
            if($account)
            {
                $transaction->account()->associate($account);
            }
        }
        if($request->transfert_id)
        {
            $transfert=\App\Transfert::find($request->transfert_id);
            if($transfert)
            {
                $transaction->transfert()->associate($transfert);
            }
        }
        
        $transaction->edited = 1;
        $transaction->save();
        $transaction= \App\Transaction::with('transfert','account')->find($id);
        return response()->json($transaction);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = \App\Transaction::find($id);
        $transaction->deleted = 1;
        $transaction->save();
        return response()->json($transaction);
    }    
}
