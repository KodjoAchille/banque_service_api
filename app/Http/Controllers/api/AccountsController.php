<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth; 
use App\Account; 
use App\Transaction; 
use App\Transfert;
use Exception; 

class AccountsController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts= \App\Account::where('deleted', false)->with('user','transactions')->orderBy('id', 'DESC')->get();
        return response()->json([
            'account'=>$accounts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            $account = new \App\Account();
            $account->account_number=$this->generateUniqueCode();
            $account->code=$request->code;
            $account->sold=$request->sold;
            $account->type=$request->type;
            $account->user()->associate($user);
            $account->save();
            return response()->json($account); 
        }
        else
        {
            //throw new Exception("erreur vérifier si c'est bien votre numero de compte");
            return response()->json([
                'status'=>401,
                'message'=>"email ou mot de passe incorrect",
            ],401);
        }
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function generateUniqueCode()
    {
        do {
            $account_number = random_int(10000000, 99999999);
        } while (\App\Account::where('account_number',$account_number)->first());
        return $account_number;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account= \App\Account::with('user','transactions')->find($id);

        return response()->json(['account'=> $account]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $account= \App\Account::find($id);
        if($request->account_number)
        {
            $account->account_number=$request->account_number;
        }
        if($request->code)
        {
            $account->code=$request->code;
        }
        if($request->sold)
        {
            $account->sold=$request->sold;
        }
        if($request->type)
        {
            $account->type=$request->type;
        }
        
        if($request->user_id)
        {
            $user=\App\User::find($request->user_id);
            if($user)
            {
                $account->user()->associate($user);
            }
        }
        $account->edited = 1;
        $account->save();
        $account= \App\Account::with('user','transactions')->find($id);
        return response()->json($account);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = \App\Account::find($id);
        $account->deleted = 1;
        $account->save();
        return response()->json($account);
    }

    //specific functions
    /**
     * get sold.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSold(Request $request)
    {
        $account = \App\Account::where('account_number', $request->account_number)->first();
        
        if($account)
        {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $currentUser = Auth::user();
                if($account->user_id == $currentUser->id)
                {
                    return response()->json([
                        'status'=>200,
                        'sold'=>$account->sold,
                    ]);
                    
                }
                else
                {
                    //throw new Exception("erreur vérifier si c'est bien votre numero de compte");
                    return response()->json([
                        'status'=>403,
                        'message'=>"erreur vérifier si c'est bien votre numero de compte",
                    ],403);
                }
            }
            else{
                return response()->json([
                    'status'=>401,
                    'message'=>"email ou mot de passe incorrect",
                ],401); 
            }
            
        }
        else
        {
            //throw new Exception("no account match with given account_number");
            return response()->json([
                'status'=>403,
                'message'=>"no account match with given account_number",
            ],403); 
        }
    }

    /**
     * get transaction history.
     *
     *  @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        $account = \App\Account::where('account_number', $request->account_number)->first();
        if(!$account)
        {
            return response()->json([
                'status'=>403,
                'message'=>"no account match with given account_number",
            ],403);
        }
        else
        {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $currentUser = Auth::user();
            
                if($account->user_id == $currentUser->id)
                {
                    $transactions= \App\Transaction::where('account_id',$account->id)->get();
                    return response()->json([
                        'status'=>200,
                        'transactions'=>$transactions,
                    ]);     
                }
                else
                {
                    return response()->json([
                        'status'=>403,
                        'message'=>"erreur vérifier si c'est bien votre numero de compte",
                    ],403);
                }
            }
            else{
                return response()->json([
                    'status'=>401,
                    'message'=>"email ou mot de passe incorrect",
                ],401); 
            }            
        }
    }

    //specific functions
    /**
     * credit account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function credit(Request $request)
    {
        $account = \App\Account::where('account_number', $request->account_number)->first();
        if(!$account)
        {
            return response()->json([
                'status'=>403,
                'message'=>"no account match with given account_number",
            ],403); 
        }
        $transaction= new \App\Transaction();
        $transaction->amount=$request->amount;
        $transaction->date=$request->date;
        $transaction->type="credit";
        $account->sold+=$request->amount;
        $account->save();
        $transaction->account()->associate($account);
        $transaction->save();
        return response()->json([
            'transaction'=>\App\Transaction::with('account')->find($transaction->id),
            'status'=>200
        ]);
    }
    //specific functions
    /**
     * debit account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function debit(Request $request)
    {
        $account = \App\Account::where('account_number', $request->account_number)->first();
        if(!$account)
        {
            return response()->json([
                'status'=>403,
                'message'=>"no account match with given account_number",
            ],403);
        }
        if($account && $request->amount >$account->sold)
        {
            return response()->json([
                'message'=>"solde insuffsant",
                'status'=>403
            ],403);
        }
        else
        {
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $currentUser = Auth::user();
            
                if($account->user_id == $currentUser->id)
                {
                    $transaction= new \App\Transaction();
                    $transaction->amount=$request->amount;
                    $transaction->date=$request->date;
                    $transaction->type="debit";
                    $account->sold-=$request->amount;
                    $account->save();
                    $transaction->account()->associate($account);
                    $transaction->save();
                    return response()->json([
                        'transaction'=>\App\Transaction::with('account')->find($transaction->id),
                        'status'=>200
                    ]);
                }
                else
                {
                    return response()->json([
                        'status'=>403,
                        'message'=>"erreur vérifier si c'est bien votre numero de compte",
                    ],403);
                }
            }
            else{
                return response()->json([
                    'status'=>401,
                    'message'=>"email ou mot de passe incorrect",
                ],401); 
            }
            
        }
      
    }

    //specific functions
    /**
     * debit account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function innerTransfert(Request $request)
    {
        $sender_account = \App\Account::where('account_number', $request->sender_account_number)->first();
        $receiver_account = \App\Account::where('account_number', $request->receiver_account_number)->first();
        if(!$receiver_account || !$sender_account)
        {
            return response()->json([
                'message'=>"l'un des numeros de compte est incorrect",
                'status'=>403
            ],403);
        }
        else
        {
            if($request->amount > $sender_account->sold)
            {
                return response()->json([
                    'message'=>"solde insuffsant",
                    'status'=>403
                ],403);
            }
            else
            {

                if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                    $currentUser = Auth::user();
                    if($sender_account->user_id == $currentUser->id)
                    {
                        //initialisation du transfert
                        $transfert = new \App\Transfert();
                        $transfert->save();
                        $transfert_id=$transfert->id;
                        //enregistrement de la transaction de debit
                        $sender_account->sold-=$request->amount;
                        $sender_account->save();
                        $transaction= new \App\Transaction();
                        $transaction->amount=$request->amount;
                        $transaction->date=$request->date;
                        $transaction->type="debit";
                        $transaction->account()->associate($sender_account);
                        $transaction->transfert()->associate($transfert);
                        $transaction->save();
                        //enregistrement de la transaction de credit
                        $receiver_account->sold+=$request->amount;
                        $receiver_account->save();
                        $transaction= new \App\Transaction();
                        $transaction->amount=$request->amount;
                        $transaction->date=$request->date;
                        $transaction->type="credit";
                        $transaction->account()->associate($receiver_account);
                        $transaction->transfert()->associate($transfert);
                        $transaction->save();
                        return response()->json([
                            'transfert'=>\App\Transfert::with('transactions')->find($transfert_id),
                            'receiver_account'=>$receiver_account,
                            'sender_account' =>$sender_account,
                            'status'=>200
                        ]);
                    }
                    else
                    {
                        return response()->json([
                            'status'=>403,
                            'message'=>"erreur vérifier si le numero de compte à débiter vous appartient",
                        ],403);
                    }
                }
                else{
                    return response()->json([
                        'status'=>401,
                        'message'=>"email ou mot de passe incorrect",
                    ],401); 
                }
            }
        }
        
    }

    //specific functions
    /**
     * debit account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function externTransfert(Request $request)
    {
        $sender_account = \App\Account::where('account_number', $request->sender_account_number)->first();
        //$receiver_account = \App\Account::where('account_number', $request->receiver_account_number)->first();
        if(!$sender_account)
        {
            return response()->json([
                'message'=>"numero de compte du l'emetteur incorrect ",
                'status'=>403
            ],403);
        }
        else
        {
            if($request->amount > $sender_account->sold)
            {
                return response()->json([
                    'message'=>"solde insuffsant",
                    'status'=>403
                ],403);
            }
            else
            {
                if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                    $currentUser = Auth::user();
                    if($sender_account->user_id == $currentUser->id)
                    {
                       //initialisation du transfert
                        $response = Http::post('Https://bankfirst.herokuapp.com/api/v1/receive', [
                            'amount'=>$request->amount,
                            'accountsender'=>$request->sender_account_number,
                            'accountreceiver'=>$request->receiver_account_number,
                            'bank'=>"banqueA"
                        ]);
                    
                        if($response->status()==201)
                        {
                            $transfert = new \App\Transfert();
                            $transfert->save();
                            $transfert_id=$transfert->id;
                            //enregistrement trace de la transaction de credit
                            $transaction= new \App\Transaction();
                            $transaction->amount=$request->amount;
                            $transaction->date=$request->date;
                            $transaction->type="credit";
                            $transaction->scope="externe";
                            $transaction->ext_account_number=$request->receiver_account_number;
                            $transaction->bank=$request->ext_bank_name;
                            $transaction->transfert()->associate($transfert);
                            $transaction->save();
                            //enregistrement de la transaction de debit
                            $sender_account->sold-=$request->amount;
                            $sender_account->save();
                            $transaction= new \App\Transaction();
                            $transaction->amount=$request->amount;
                            $transaction->date=$request->date;
                            $transaction->type="debit";
                            $transaction->account()->associate($sender_account);
                            $transaction->transfert()->associate($transfert);
                            $transaction->save();
                            return response()->json([
                                'transfert'=>\App\Transfert::with('transactions')->find($transfert_id),
                                'sender_account' =>$sender_account,
                                'status'=>200
                            ]);
                        }
                        else {
                            return response()->json([
                                'status'=>403,
                                'message'=>"erreur vérifier si le numero de compte à débiter vous appartient",
                            ],403);
                        } 
                    }
                    else
                    {
                        return response()->json([
                            'status'=>403,
                            'message'=>"erreur vérifier si le numero de compte à débiter vous appartient",
                        ],403);
                    }
                }
                else{
                    return response()->json([
                        'status'=>401,
                        'message'=>"email ou mot de passe incorrect",
                    ],401); 
                }
                
            }
        }
        
    }
    
}