<?php
namespace App\Http\Controllers\api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Transfert; 

class TranfertsController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts= \App\Account::where('deleted', false)->with('transactions')->orderBy('id', 'DESC')->get();
        return response()->json([
            'transferts'=>$transferts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transfert = new \App\Transfert();
        $transfert->save();
        return response()->json($transfert);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transfert= \App\Transfert::with('transactions')->find($id);

        return response()->json(['transfert'=> $transfert]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transfert = \App\Transfert::find($id);
        $transfert->deleted = 1;
        $transfert->save();
        return response()->json($transfert);
    }
    
}
