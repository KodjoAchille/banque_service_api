<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table="transactions";
    protected $timestamp = true;

    public function transfert(){
        return $this->belongsTo('App\Transfert');
    }

    public function account(){
        return $this->belongsTo('App\Account');
    }
}
